'use strict';

let express = require('express');
let bodyParser = require('body-parser')

var app = express();
var http = require('http').Server(app);





/////////%%%%%%%% IMPORTANTE

app.get('/', function (req, res) {
	console.log("ENVIANDO ARCHIVO index.html");
	res.sendFile(__dirname + '/client/index.html');
});

app.get('/edit', function (req, res) {
	console.log("ENVIANDO ARCHIVO edit.html");
	res.sendFile(__dirname + '/client/edit.html');
});

app.get('/damewachin', function (req, res) {
	console.log("Request recibido para obtener usuario", req.query);
	res.send({ nombre: "Wachin", edad: "666" })
});




app.use('/client', express.static(__dirname + '/client'));
app.use(bodyParser.urlencoded({
	extended: false
}));
app.use(bodyParser.json());

http.listen(3000, function (err) {
	if (err)
		console.log(JSON.stringify(err));
	else
		console.log("The server has started successfully on port: 3000");
});